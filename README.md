This app is a console application built with C#
# How to run this code
1. Download or Clone this repository
2. Run Assignment1.sln with Visual Studio
3. Click "Run" or press F5 to run the app

# Why I selected MIT License
1. It is short, simple and permissive
2. Any developers could modify and reuse my code

# Project Wiki
A Capstone customer might prefer private repository to public ones mostly because they want to retain the privacy of the code. 
Additionally, if the project is not open source, the customer will definitely choose to use the private repository.
Signal Private Messenger would be open source because the main point of Signal is privacy and the advantage of open source is the security of the application.
