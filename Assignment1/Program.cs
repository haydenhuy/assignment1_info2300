﻿/* 
 * Program Name: Assignment 1
 * 
 * By Huy Pham
 * 
 * Revision history
 *      created by Huy in Sep. 2020
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    class Program
    {
        static void Main(string[] args)
        {
            //declaration
            string temp;
            double tempDouble;
            double tempInFahrenheit;

            //initialize
            temp = "0";
            tempDouble = 0;
            tempInFahrenheit = 0;

            //input
            Console.Write("Input temperature in Celsius: ");
            temp = Console.ReadLine();
            tempDouble = Convert.ToDouble(temp);

            //calculation
            tempInFahrenheit = tempDouble * 1.8 + 32;

            //information output
            Console.WriteLine();
            Console.WriteLine("Huy Pham - Assignment 1 Part 2");
            Console.WriteLine();
            Console.WriteLine("Name: Huy Pham");
            Console.WriteLine("Email: hpham4280@conestogac.on.ca");
            Console.WriteLine("The Celsius temperature of " + tempDouble + " degrees converts to " + tempInFahrenheit + " in Fahrenheit");
            Console.WriteLine("Experience: Low");
            Console.WriteLine("Country: Vietnam");

            Console.ReadLine();
        }
    }
}
